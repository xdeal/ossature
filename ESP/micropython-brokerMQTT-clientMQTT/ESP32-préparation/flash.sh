#!/bin/bash

FLASH_COMPORT="/dev/ttyUSB0"
FLASH_BDRATE="460800"
VERSION="esp32-idf3-20191220-v1.12.bin"

esptool.py --port ${FLASH_COMPORT} erase_flash
echo "erase flash!!!!"
sleep 5
esptool.py --chip esp32 --port ${FLASH_COMPORT} write_flash -z 0x1000 ${VERSION}
echo "load firmware!!!!"
sleep 15
screen /dev/ttyUSB0 115200
