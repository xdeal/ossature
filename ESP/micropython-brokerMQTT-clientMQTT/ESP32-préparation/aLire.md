
#Préparation de l'ESP32
### pour téléverser le [firmware](https://micropython.org/download/esp32/) micropython dans l'[esp32](https://docs.micropython.org/en/latest/esp32/tutorial/intro.html) il faut le module esptool 

```bash
pip install esptool
```

### téléchargement du firmware:
```bash

esptool.py --port /dev/ttyUSB0 erase_flash
```

```bash

esptool.py --chip esp32 --port /dev/ttyUSB0  write_flash -z 0x1000 esp32-idf3-20191220-v1.12.bin
```
#### pour verifier: 
```shell

screen /dev/ttyUSB0 115200
```  
 **(appuyer sur le bouton reset de l'ESP)**

#### script pour enchainer toutes les commandes précédentes:
```shell

./flash.sh
```

