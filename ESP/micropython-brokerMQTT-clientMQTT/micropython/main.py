# Complete project details at https://RandomNerdTutorials.com
#https://docs.micropython.org/en/latest/esp32/quickref.html
import time
from umqtt.robust import MQTTClient
import machine
from machine import Pin, I2C, ADC
import micropython
import network
import esp
esp.osdebug(None)
import gc
gc.collect()
import ubinascii
client_id = ubinascii.hexlify(machine.unique_id())

############# choisir le broker #######################
#mqtt_server = '192.168.43.250'
mqtt_server = "mqtt.eclipse.org"
#######################################################

rtc = machine.RTC()
rtc.datetime((2020, 2, 29, 0, 0, 0, 0, 0))#se reinitialise à chaque coupure d'alimentation mais pas lors d'un reset

def sub_cb(topic, msg):  # trier les sujets rentrants
    print((topic, msg))
    if topic == b'ossature/actionneur/voyant/v1' and msg == b'OFF':
        print('ESP received v1:OFF ')
        p2.on()#sortie active à l'état bas
    if topic == b'ossature/actionneur/voyant/v1' and msg == b'ON':
        print('ESP received v1:ON')
        p2.off()#sortie active à l'état bas

def connect_and_subscribe():
    global client_id, mqtt_server
    client = MQTTClient(client_id, mqtt_server)
    client.set_callback(sub_cb)
    client.connect()
    client.subscribe("ossature/actionneur/voyant/v1")
    print('Connected to %s MQTT broker' % (mqtt_server))
    return client

def restart_and_reconnect():
    print('Failed to connect to MQTT broker. Reconnecting...')
    time.sleep(3)
    machine.reset()
    
def publieMessage(topic,mess):
    tupl = rtc.datetime()#(2020, 2, 29, 5, 0, 5, 49, 39490)->2020-09-18 09:46:35.628870
    chaine = str(tupl[0])+"-"+str(tupl[1])+"-"+str(tupl[2])+" "+str(tupl[3])+":"+str(tupl[4])+":"+str(tupl[5])+":"+str(tupl[6])+"."+str(tupl[7])
    horodatage = chaine
    mess = mess + ";" + horodatage #format ossature
    client.publish(topic, mess)
    return mess
    
try:
    client = connect_and_subscribe()
except OSError as e:
    restart_and_reconnect()
        
# -------------debut interruption------------------------------------
# non utilisé ici
pir = Pin(18, Pin.IN, Pin.PULL_UP)
compteur = 0

def handle_interrupt(pin):
    global compteur
    compteur = compteur+1
    print("compteur: ", compteur)#à commenter
    
pir.irq(trigger=Pin.IRQ_RISING, handler=handle_interrupt)
#state = machine.disable_irq()
#print("compteur: ", compteur)
#machine.enable_irq(state)

# -------------fin interruption--------------------------------------
# --------------debut programme principal----------------------------
p2 = Pin(22, Pin.OUT)    # create output pin on GPIO0 (LED)
adc = ADC(Pin(33))          # create ADC object on ADC pin
adc.atten(ADC.ATTN_11DB)    # set 11dB input attentuation (voltage range roughly 0.0v - 3.6v)
adc.width(ADC.WIDTH_12BIT)   # set 9 bit return values (returned range 0-4095)
analog_old=0

def bouclePrincipale():
    global analog_old
    analog=adc.read()   
    if analog_old != analog:#pour préserver la bande passante
        analog_old = analog #envoyer si different
        ana=str(analog)
        print("valeur analogique: ",ana)
        publieMessage(b'ossature/capteur/temperature/t1',ana)
    else:
        pass
 
print("boucle lancée")
while True:
    try:
        client.check_msg()
        bouclePrincipale()
    except OSError as e:
        restart_and_reconnect()

if __name__ == "__main__":
    pass
