## Projet ossature partie IoT

1. pour préparer l'ESP32 voir le dossier **ESP32-préparation**
2. pour le projet ossature il suffit de téléverser les programmes du dossier **micropython** en vous aidant de la documentation contenue dans le dossier **rshell** et d'alimenter L'ESP. Attention de ne pas oublier de compléter le point d'accès dans le fichier boot.py.
3. pour les essais, utiliser MQTTBox ou le clientHTML du dossier *Clients* (voir ossaturePresentationMicropythonMqttHtml.odt), ou alors le serveur Flask (voir ossaturePresentationMicropythonMqttFlask.odt)
4. pour un fonctionnement réel, alimenter l'ESP et vérifiez que le broker configuré dans main.py soit identique aux autres clients MQTT du projet Ossature.


_____

#### Pense-bête et références:
**pour linux configuration du broker mosquitto**
[Using MQTT Over WebSockets with Mosquitto](http://www.steves-internet-guide.com/mqtt-websockets/)
*dans:*
/etc/mosquitto/mosquitto.conf
*vérifier:*
listener 1883
protocol mqtt
listener 9001
protocol websockets

[mosquitto.conf man page](https://mosquitto.org/man/mosquitto-conf-5.html)

[Paho JavaScript-très synthétique](https://www.hivemq.com/blog/mqtt-client-library-encyclopedia-paho-js/)

### redemarrer mosquitto
systemctl restart mosquitto
