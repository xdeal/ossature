


##Rshell
[Remote MicroPython shell](https://github.com/dhylands/rshell)
### 1.  Se connecter au pyboard(console)
```shell

rshell --port /dev/ttyUSB0 --baud 115200 --buffer-size 128 --editor nano
```
**to exit the shell CTRL+C**
________________________________


### 2.  liste les carte et les repertoires(rshell)
```shell

boards
```
### copier un fichier(rshell)
```shell

cp blynk.py /pyboard
```
### synchroniser tous les fichiers(rshell)
```shell

rsync . /pyboard
```
### effacer les fichiers/dossier de l'ESP32(rshell)
```shell

rm -r /pyboard/*
```
**to exit the rshell press CTRL+D**
_______________________________
   
### 3.  lancer l'interpreteur(repl)
```shell

repl
```

_______
### 4. lancement du programme:
* soit dans l'invite de commande du repl vous tapez: 
```shell
import boot
import main
```
* soit vous appuyez sur le bouton reset de l'ESP

________
lance l'interpreteur boucle de lecture execution ecriture
### avec passage d'une commande python et sortie de repl (optionnel)
```shell

repl [nom-de-la-carte] [~ ligne][ ~]
repl ~ globals() locals()  
```
**to exit the repl press CTRL+X**
_________________________
### lancer un script (ex: start.py) en important un module(repl)
```shell

import start
```
**to exit the repl press CTRL+X**