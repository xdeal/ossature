Se mettre en condition avec un [diaporama](https://www.slideshare.net/lguerin/mqtt-avec-mosquitto-et-paho-devfest-brest-2019).

Comprendre la logique des sujets (topic) avec les caractères spéciaux [wildcard](https://mosquitto.org/man/mqtt-7.html).


installer le broker MQTT [mosquitto](https://mosquitto.org/download/).
La page [github](https://github.com/eclipse/mosquitto) pour s'initier grâce à la partie [quick start](https://github.com/eclipse/mosquitto#quick-start).

Pour construire des clients MQTT en python nous utiliserons [paho-mqtt-python](https://github.com/eclipse/paho.mqtt.python), il faudra parcourir son API et l'installer avec un 'pip install paho-mqtt'.

