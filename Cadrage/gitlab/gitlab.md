**Pense-bête Gitlab**
_________________________________________


**site**
https://framagit.org/public/projects
___________________________________________


**Pour cloner projet distant se mettre dans un dossier d'arrivée**


***en ssh***

git clone git@framagit.org:xdeal/ossature.git


***en HTTPS***

git clone https://framagit.org/xdeal/ossature.git

__________________________________________________________________

**les plus utilisés pour push (mettre à jour le remote)**

git status

git add .

git commit -m "commentaires"


git push


**les plus utilisés pour push (MàJ du repertoire local)**

git status

git pull


___________________________________________________________________


**récupérer lors de la création du dépot**
***Command line instructions***

You can also upload existing files from your computer using the instructions below.
Git global setup

git config --global user.name "XD"
git config --global user.email "xavier.deal@ac-lyon.fr"



Create a new repository

git clone git@framagit.org:xdeal/ossature.git
cd essai
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master



Push an existing folder

cd existing_folder
git init
git remote add origin git@framagit.org:xdeal/ossature.git
git add .
git commit -m "Initial commit"
git push -u origin master



Push an existing Git repository

cd existing_repo
git remote rename origin old-origin
git remote add origin git@framagit.org:xdeal/ossature.git
git push -u origin --all
git push -u origin --tags





***video formative***
https://www.youtube.com/watch?v=ySLhc3qdGEk



