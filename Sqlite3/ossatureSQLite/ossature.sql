BEGIN TRANSACTION;
-- Activation des clefs étrangères
PRAGMA foreign_keys=1; 
DROP TABLE IF EXISTS `capteur`;
CREATE TABLE IF NOT EXISTS `capteur` (
	`nomCapteur`	TEXT,
	`refCapteur`	TEXT,
	`grandeurPhysique`	TEXT,
	`unite`	TEXT,
	`topic`	TEXT,
	PRIMARY KEY(`nomCapteur`)
);
DROP TABLE IF EXISTS `localisation`;
CREATE TABLE IF NOT EXISTS `localisation` (
	`coordGPScapteur`	TEXT,
	`nomCapteur`	TEXT,
	`zone`	TEXT,
	`infoZone`	TEXT,
	FOREIGN KEY(`nomCapteur`) REFERENCES `capteur`(`nomCapteur`),
	PRIMARY KEY(`coordGPScapteur`,`nomCapteur`)
);
DROP TABLE IF EXISTS `mesure`;
CREATE TABLE IF NOT EXISTS `mesure` (
	`horodatage`	TEXT,
	`nomCapteur`	TEXT,
	`valeur`	INTEGER,
	PRIMARY KEY(`horodatage`,`nomCapteur`),
	FOREIGN KEY(`nomCapteur`) REFERENCES `capteur`(`nomCapteur`)
);
INSERT INTO `capteur` (nomCapteur,refCapteur,grandeurPhysique,unite,topic) VALUES ('t1','HR202','temperature','C','ossature/capteur/temperature/t1');
INSERT INTO `capteur` (nomCapteur,refCapteur,grandeurPhysique,unite,topic) VALUES ('t2','DS18B20','temperature','C','ossature/capteur/temperature/t2');
INSERT INTO `capteur` (nomCapteur,refCapteur,grandeurPhysique,unite,topic) VALUES ('t3','DS18B20','temperature','C','ossature/capteur/temperature/t3');
INSERT INTO `capteur` (nomCapteur,refCapteur,grandeurPhysique,unite,topic) VALUES ('t4','HR203','temperature','K','ossature/capteur/temperature/t4');
INSERT INTO `capteur` (nomCapteur,refCapteur,grandeurPhysique,unite,topic) VALUES ('p1','MPX5010','pression','hPa','ossature/capteur/pression/p1');
INSERT INTO `capteur` (nomCapteur,refCapteur,grandeurPhysique,unite,topic) VALUES ('t5','LM35','temperature','K','ossature/capteur/temperature/t5');
INSERT INTO `capteur` (nomCapteur,refCapteur,grandeurPhysique,unite,topic) VALUES ('hr1','HPP80','humidite','%','ossature/capteur/hr/hr1');
INSERT INTO `capteur` (nomCapteur,refCapteur,grandeurPhysique,unite,topic) VALUES ('t6','BME280','temperature','C','ossature/capteur/temperature/t6');
INSERT INTO `capteur` (nomCapteur,refCapteur,grandeurPhysique,unite,topic) VALUES ('hr6','BME280','humidite','%','ossature/capteur/hr/hr6');
INSERT INTO `capteur` (nomCapteur,refCapteur,grandeurPhysique,unite,topic) VALUES ('p6','BME280','pression','hPa','ossature/capteur/pression/p6');

INSERT INTO `mesure` (horodatage,nomCapteur,valeur) VALUES ('2020-06-18 09:46:35.628870','t2',22.9);
INSERT INTO `mesure` (horodatage,nomCapteur,valeur) VALUES ('2020-06-03 05:59:18.569874','t3',8.2);
INSERT INTO `mesure` (horodatage,nomCapteur,valeur) VALUES ('2019-07-08 05:59:18.569874','t1',10.5);
INSERT INTO `mesure` (horodatage,nomCapteur,valeur) VALUES ('2020-09-18 09:46:35.628870','t1',13.78);
INSERT INTO `mesure` (horodatage,nomCapteur,valeur) VALUES ('2018-06-18 09:46:35.628870','t1',12.62);
INSERT INTO `mesure` (horodatage,nomCapteur,valeur) VALUES ('2020-06-18 09:46:35.628879','t1',12.65);
INSERT INTO `mesure` (horodatage,nomCapteur,valeur) VALUES ('2020-06-18 09:49:35.628870','t1',10.25);
INSERT INTO `mesure` (horodatage,nomCapteur,valeur) VALUES ('2020-06-18 09:46:39.628870','t1',9.22);
INSERT INTO `mesure` (horodatage,nomCapteur,valeur) VALUES ('2020-06-19 09:46:35.628870','t1',-9.5);
INSERT INTO `mesure` (horodatage,nomCapteur,valeur) VALUES ('2020-06-18 00:46:35.628870','t1',0);
INSERT INTO `mesure` (horodatage,nomCapteur,valeur) VALUES ('2018-06-18 09:49:35.628870','t6',21);
INSERT INTO `mesure` (horodatage,nomCapteur,valeur) VALUES ('2018-06-18 09:46:35.928870','p6',2314);
INSERT INTO `mesure` (horodatage,nomCapteur,valeur) VALUES ('2018-06-18 09:46:39.628870','hr6',38);

INSERT INTO `localisation` (coordGPScapteur,nomCapteur,zone,infoZone) VALUES ('N5O9','t1','marais','attention aux moustiques');
INSERT INTO `localisation` (coordGPScapteur,nomCapteur,zone,infoZone) VALUES ('S7O8','t2','potager','attention aux coquelicots');
INSERT INTO `localisation` (coordGPScapteur,nomCapteur,zone,infoZone) VALUES ('N4O3','t3','poulailler','attention au coq');
INSERT INTO `localisation` (coordGPScapteur,nomCapteur,zone,infoZone) VALUES ('N5O9','t4','serre','roses de collection');
INSERT INTO `localisation` (coordGPScapteur,nomCapteur,zone,infoZone) VALUES ('N7S8','t5','garage','sous-sol');
INSERT INTO `localisation` (coordGPScapteur,nomCapteur,zone,infoZone) VALUES ('N8E6','t6','vivarium','vers à soie');
INSERT INTO `localisation` (coordGPScapteur,nomCapteur,zone,infoZone) VALUES ('N8E6','p6','vivarium','vers à soie');
INSERT INTO `localisation` (coordGPScapteur,nomCapteur,zone,infoZone) VALUES ('N8E6','hr6','vivarium','vers à soie');
INSERT INTO `localisation` (coordGPScapteur,nomCapteur,zone,infoZone) VALUES ('S5E3','p1','local2','cuisine');
INSERT INTO `localisation` (coordGPScapteur,nomCapteur,zone,infoZone) VALUES ('S9O5','hr1','local3','couloir');

COMMIT;
