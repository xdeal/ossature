*Protocole d'essai de la partie MQTT SQL*

1 **pour la Base de Données SQL:**
Soit utiliser la base ossature.db 
Soit la reconstruire avec le script ossature.sql
Soit la reconstruire avec le script python ossature.py

2 **pour la partie client MQTT SQL en autonomie:**
Lancer ossatureDB.py pour faire le lien entre le Broker et la base de données.

3 **pour la partie capteur en autonomie:**
Lancer clientPubSub.py pour simuler le travail des capteurs et remplir la base de données.

4 **pour la partie requetes SQL en autonomie:**
Utiliser clientPubSub_SQL.py pour interroger via le broker la base de données et recevoir la réponse.
mettre  modeStandAlone = True pour simuler des requêtes extérieures

**pour le fonctionnement complet: capteurs-requeteSQL-réponse base de données:**
Utiliser clientPubSub_SQL.py et décommenter les parties à utilisées.
mettre  modeStandAlone = False pour souscrire aux requêtes extérieures




