﻿import sqlite3

conn = sqlite3.connect('baseDonnees.db')
conn.execute("PRAGMA foreign_keys = 1")
cur = conn.cursor()

datas = [
	('t1','HR202','temperature','C','ossature/capteur/temperature/t1'),
	('t2','DS18B20','temperature','C','ossature/capteur/temperature/t2'),
	('t3','DS18B20','temperature','C','ossature/capteur/temperature/t3'),
	('t4','HR203','temperature','K','ossature/capteur/temperature/t4'),
	('p1','MPX5010','pression','hPa','ossature/capteur/pression/p1'),
	('t5','LM35','temperature','K','ossature/capteur/temperature/t5'),
	('hr1','HPP80','humidite','%','ossature/capteur/hr/hr1'),
	('t6','BME280','temperature','C','ossature/capteur/temperature/t6'),
	('hr6','BME280','humidite','%','ossature/capteur/hr/hr6'),
	('p6','BME280','pression','hPa','ossature/capteur/pression/p6')
 ]

datas2 =[
    ('N5O9','t1','marais','attention aux moustiques'),
    ('S7O8','t2','potager','attention aux coquelicots'),
    ('N4O3','t3','poulailler','attention au coq'),
    ('N5O9','t4','serre','roses de collection'),
    ('N7S8','t5','garage','sous-sol'),
    ('N8E6','t6','vivarium','vers à soie'),
    ('N8E6','p6','vivarium','vers à soie'),
    ('N8E6','hr6','vivarium','vers à soie'),
    ('S5E3','p1','local2','cuisine'),
    ('S9O5','hr1','local3','couloir')
 ]

datas3 =[
    ('2020-06-18 09:46:35.628870','t2',22.9),
    ('2020-06-03 05:59:18.569874','t3',8.2),
    ('2019-07-08 05:59:18.569874','t1',10.5),
    ('2020-09-18 09:46:35.628870','t1',13.78),
    ('2018-06-18 09:46:35.628870','t1',12.62),
    ('2020-06-18 09:46:35.628879','t1',12.65),
    ('2020-06-18 09:49:35.628870','t1',10.25),
    ('2020-06-18 09:46:39.628870','t1',9.22),
    ('2020-06-19 09:46:35.628870','t1',-9.5),
    ('2020-06-18 00:46:35.628870','t1',0),
    ('2018-06-18 09:49:35.628870','t6',21),
    ('2018-06-18 09:46:35.928870','p6',2314),
    ('2018-06-18 09:46:39.628870','hr6',38),
 ]

cur.execute("DROP TABLE IF EXISTS CAPTEUR")
cur.execute("DROP TABLE IF EXISTS localisation")
cur.execute("DROP TABLE IF EXISTS mesure")
            
cur.execute("CREATE TABLE CAPTEUR(nomCapteur TXT PRIMARY KEY,refCapteur TXT,grandeurPhysique TXT,unite TXT,topic TXT)")
cur.executemany("INSERT INTO CAPTEUR(nomCapteur, refCapteur, grandeurPhysique, unite, topic) VALUES(?, ?, ?, ?, ?)", datas)

cur.execute("CREATE TABLE localisation(coordGPScapteur TXT, nomCapteur TXT, zone TXT, infoZone TXT, FOREIGN KEY(nomCapteur) REFERENCES CAPTEUR(nomCapteur), PRIMARY KEY(coordGPScapteur, nomCapteur))")
cur.executemany("INSERT INTO localisation(coordGPScapteur,nomCapteur,zone,infoZone) VALUES(?, ?, ?, ?)", datas2)

cur.execute("CREATE TABLE mesure(horodatage TXT, nomCapteur TXT, valeur INT, FOREIGN KEY(nomCapteur) REFERENCES CAPTEUR(nomCapteur), PRIMARY KEY(horodatage, nomCapteur))")
cur.executemany("INSERT INTO mesure(horodatage,nomCapteur,valeur) VALUES(?, ?, ?)", datas3)

cur.execute('SELECT nomCapteur FROM Capteur')

conn.commit()

cur.close()
conn.close()


