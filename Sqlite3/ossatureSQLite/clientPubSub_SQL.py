#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# client pour base de donnée sqlite3
# permet de souscrire à une requete et 
# de publier la réponse
# en protocole MQTT
#
#           projet ossature

import sqlite3
import logging
import paho.mqtt.publish as publish
import paho.mqtt.client as mqtt
import time
import datetime
import json #pour serialiser

modeStandAlone=False # True pour faire des essais de connection à la BD par l'intermédiaire du broker uniquement
                     # False pour le mode connecté avec le serveur par l'intermédiaire du broker
# mettre level=logging.DEBUG pour avoir plus d'informations
logging.basicConfig(level=logging.INFO)

############ choisir le broker ############################################
broker= "mqtt.eclipse.org"
#broker = "localhost"
logging.info("choix du broker: %s ", broker) 
############# vérifier la présence du fichier dans le dossier #############
DB_FILE = 'ossature.db'
#######################################################
requete = "select * from mesure"
# requete = "select * \
#            from capteur\
#            join mesure on capteur.nomCapteur = mesure.nomCapteur\
#            join localisation on capteur.nomCapteur = localisation.nomCapteur\
#            where horodatage > '2018-07' and horodatage < '2020'\
#            order by horodatage"

def on_message_capteur_temperature(self, obj, msg):
    # This callback will only be called for messages with topics that match
    # ossature/capteur/temperature/#
    valeur, horodatage = payload_byteToList(msg.payload)
    nomCapteur = recupDernier(topicToList(msg.topic))
    logging.debug("CAPTEUR temperature: %s  QoS: %s  payload: %s ", nomCapteur , str(msg.qos) , str(msg.payload))
    requete = f"INSERT INTO `mesure` (horodatage,nomCapteur,valeur) VALUES ('{horodatage}','{nomCapteur}',{valeur});"
    ecrireOssatureBD(requete) 
    
def on_message_capteur_hr(self, obj, msg):
    # This callback will only be called for messages with topics that match
    # ossature/capteur/hr/#
    valeur, horodatage = payload_byteToList(msg.payload)
    nomCapteur = recupDernier(topicToList(msg.topic))
    logging.debug("CAPTEUR hr: %s  QoS: %s  payload: %s ", nomCapteur , str(msg.qos) , str(msg.payload))
    requete = f"INSERT INTO `mesure` (horodatage,nomCapteur,valeur) VALUES ('{horodatage}','{nomCapteur}',{valeur});"
    ecrireOssatureBD(requete) 
    
def on_message_capteur_pression(self, obj, msg):
    # This callback will only be called for messages with topics that match
    # ossature/capteur/pression/#
    valeur, horodatage = payload_byteToList(msg.payload)
    nomCapteur = recupDernier(topicToList(msg.topic))
    logging.debug("CAPTEUR pression: %s  QoS: %s  payload: %s ", nomCapteur , str(msg.qos) , str(msg.payload))
    requete = f"INSERT INTO `mesure` (horodatage,nomCapteur,valeur) VALUES ('{horodatage}','{nomCapteur}',{valeur});"
    ecrireOssatureBD(requete) 

def on_message_actionneur(self, obj, msg):
    # This callback will only be called for messages with topics that match
    # ossature/actionneur/voyant/#
    nomCapteur = recupDernier(topicToList(msg.topic))
    logging.debug("ACTIONNEUR: %s  QoS: %s  payload: %s ", nomCapteur , str(msg.qos) , str(msg.payload))
    
def on_message_sql_req(self, obj, msg):
    # traite la requete reçue sur le sujet: ossature/message/sql/requete
    # et publie le résultat sérialisé sur le sujet: ossature/message/sql/reponse
    valReq, horodatage = payload_byteToList(msg.payload)
    logging.debug("SQL_REQ  requete: %s  horodatage: %s ",  valReq , horodatage)
    logging.info("une requete reçue !")
    resultatReq = lireOssatureBD(valReq)
    reponse = json.dumps(resultatReq) # json.loads( ) pour deserialiser
    #publier la liste de tuple resultat de la requete
    date = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S.%f')
    publish.single("ossature/message/sql/reponse/r1", reponse , hostname=broker)
    logging.debug("SQL_reponse publie: %s  date: %s  ", reponse, date ) 
    
def on_message_sql_reponse(self, obj, msg):
    # affiche la reponse reçue sur le sujet: ossature/message/sql/reponse
    reponse = json.loads(msg.payload)
    logging.info("SQL_reponse recue: %s ", reponse) 


def on_message_texte(self, obj, msg):
    # This callback will only be called for messages with topics that match
    # ossature/message/sql/requete
    logging.debug("TEXTE: %s  QoS: %s  payload: %s ", msg.topic , str(msg.qos) , str(msg.payload))

def on_message(self, obj, msg):
    # This callback will be called for messages that we receive that do not
    # match any patterns defined in topic specific callbacks
    logging.debug("MESSAGE NON CONFORME: %s  QoS: %s  payload: %s ", msg.topic , str(msg.qos) , str(msg.payload))

def payload_byteToList(payload_byte):
    '''permet de parser dans une liste les différentes parties de la charge utile'''
    #convertir un tableau de byte en str
    payload_str = payload_byte.decode('UTF-8')
    liste = payload_str.split(';')#la valeur et l'horodatage sont séparés par un ;
    if len(liste) == 1: liste.append("2020-00-00 15:16:17.117114" )#dans le cas d'une requete sql
    return liste

def topicToList(msg):
    '''permet de parser le topic dans une liste, les différentes parties de la charge utile'''
    liste = msg.split('/')
    return liste

def recupDernier(topicListe):
    '''retourne le nom du capteur'''
    dernier=topicListe[-1]
    return str(dernier)

def lireOssatureBD(req="select * from capteur"):
    """reçoit la requete et renvoit le résultat"""
    result=[" "]
    try:
        connection = sqlite3.connect(DB_FILE)
        cursor = connection.cursor()
        logging.info(" You are connected to - %s", DB_FILE)
        cursor.execute(req)
        result = cursor.fetchall()
    except (sqlite3.Error) as error:
        logging.error("Error while connecting to sqlite3 in lireOssatureBD: %s", error)
        result=[" "]
    finally:
        # closing database connection.
        if connection:
            cursor.close()
            connection.close()
            logging.debug("in lireOssatureBD connection is closed in lireOssatureBD ")
        return result
            
def ecrireOssatureBD(inser=""):
    """inserer dans la DB"""
    try:
        connection = sqlite3.connect(DB_FILE)
        cursor = connection.cursor()
        logging.debug("ecrireOssatureBD You are connected to - %s", DB_FILE)
        cursor.execute(inser)
        connection.commit()  
    except (sqlite3.Error) as error:
        logging.error("ecrireOssatureBD Error while connecting to sqlite3: %s", error)
    finally:
        # closing database connection.
        if connection:
            cursor.close()
            connection.close()
            logging.debug("ecrireOssatureBD  Sqlite3 connection is closed")
            

if __name__ == "__main__":   
    try:
        mqttc = mqtt.Client()
        # # Add message callbacks that will only trigger on a specific subscription match.
        mqttc.message_callback_add("ossature/capteur/temperature/#", on_message_capteur_temperature)
        # mqttc.message_callback_add("ossature/capteur/hr/#", on_message_capteur_hr)
        # mqttc.message_callback_add("ossature/capteur/pression/#", on_message_capteur_pression)
        # mqttc.message_callback_add("ossature/actionneur/voyant/#", on_message_actionneur)
        # mqttc.message_callback_add("ossature/actionneur/moteur/#", on_message_actionneur)
        mqttc.message_callback_add("ossature/message/sql/requete/#", on_message_sql_req)
        if modeStandAlone:
            #on souscrit à la réponse
            mqttc.message_callback_add("ossature/message/sql/reponse/#", on_message_sql_reponse)
        mqttc.on_message = on_message
        mqttc.connect(broker, 1883, 60)
        mqttc.subscribe("ossature/#", 0)
        # on publie la requete
        date = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S.%f')
        if modeStandAlone:
            logging.info("envoi requete: %s ", requete) 
            publish.single("ossature/message/sql/requete", requete + ';' + date, hostname=broker)
        while True:
            mqttc.loop(0.1)# gestion des buffers
            
            
    except KeyboardInterrupt:#  CTRL+C sous linux pour arreter
        time.sleep(0.1)
        print("_____FIN______")
    
