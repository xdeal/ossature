
# Installation de  Node-red sur linux

## Installer node-red.

```shell
sudo apt-get update
sudo apt-get install nodejs npm
sudo npm install -g --unsafe-perm node-red
```

## lancer node-red

```shell
node-red
```

puis dans le terminal **CTRL+CLICK-Gauche** sur l'adresse IP du serveur pour l'ouverture de la page d'index.


exemple: 
http://127.0.0.1:1880  pour l'édition du *flow*.
http://127.0.0.1:1880/ui  pour visualiser l'interface graphique du *flow*.

## importer/ exporter les flows
Les flows sont stockés dans un fichier au format JSON. Un flow exemple est fourni dans le dossier. il permet de se connecter au topic MQTT ossature.

Pour comprendre les sauvegardes de node-red:
 https://projetsdiy.fr/node-red-sauvegarde-automatique-flows-email-ftp-locale/
