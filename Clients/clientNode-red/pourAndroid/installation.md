# Installation de  Node-red sur Android

## Installer termux.

disponible sur le Play Store d'android

https://play.google.com/store/apps/details?id=com.termux&hl=fr


## Installer node-red.
Suivre les recommandations du site suivant:
https://mpolinowski.github.io/node-red-on-android

sous termux:
```shell
apt update && apt upgrade
apt install coreutils nano nodejs
npm install -g --unsafe-perm node-red

```

## lancer node-red

```shell
node-red
```
Entrer http://127.0.0.1:1880 dans un navigateur pour avoir accès à l'interface web.

L'écran du smartphone ne permet pas de travailler confortablement, il est possible de déporter l'affichage sur l'écran d'un PC appartenant au même sous-réseau que le téléphone.

Sur le smartphone Android:

se connecter à la borne wifi du sous-réseau contenant le PC

        paramètres 
        - wifi 
        - activer wifi 
        - entrer le SSID et le MDP 
        - puis recopier l'IP du smartphone

l'interface graphique web du smartphone est maintenant disponible sur le PC à l'adresse: 
 http://<phone_ip_address>:1880

 ## importer/ exporter les flows
Les flows sont stockés dans un fichier au format JSON. Un flow exemple est fourni dans le dossier. il permet de se connecter au topic MQTT ossature.

Pour comprendre les sauvegardes de node-red:
 https://projetsdiy.fr/node-red-sauvegarde-automatique-flows-email-ftp-locale/