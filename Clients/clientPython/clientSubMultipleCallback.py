#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# exemple pour montrer l'appel de fonction callback en fonction des
# (topic) sujets reçus

#import context  # Ensures paho is in PYTHONPATH
import paho.mqtt.client as mqtt
import datetime
import time

############choisir le broker######
broker= "mqtt.eclipse.org"
#broker = "localhost"
###################################

def on_message_capteur_temperature(self, obj, msg):
    # This callback will only be called for messages with topics that match
    # ossature/capteur/temperature/#
    print("CAPTEUR: " + msg.topic + " " + str(msg.qos) + " " + str(msg.payload))
    liste = payload_byteToList(msg.payload)
    print("******contenu du payload partie1:  "+liste[0]+" , partie2: "+liste[1])
    date_time = datetime.datetime.strptime(liste[1], '%Y-%m-%d %H:%M:%S.%f')
    print("******dans la partie 2: "+date_time.strftime('%Y-%m-%d %H:%M:%S.%f')+
          " on peut recuperer l'année:  "+str(date_time.year)+
          " les minutes: "+str(date_time.minute)+" voir les microsecondes: "+
           str(date_time.microsecond)
         )
    
def on_message_capteur_hr(self, obj, msg):
    # This callback will only be called for messages with topics that match
    # ossature/capteur/hr/#
    print("CAPTEUR: " + msg.topic + " " + str(msg.qos) + " " + str(msg.payload))
    
def on_message_capteur_pression(self, obj, msg):
    # This callback will only be called for messages with topics that match
    # ossature/capteur/pression/#
    print("CAPTEUR: " + msg.topic + " " + str(msg.qos) + " " + str(msg.payload))

def on_message_actionneur(self, obj, msg):
    # This callback will only be called for messages with topics that match
    # ossature/actionneur/voyant/#
    print("ACTIONNEUR: " + msg.topic + " " + str(msg.qos) + " " + str(msg.payload))
    
def on_message_sql_req(self, obj, msg):
    # This callback will only be called for messages with topics that match
    # ossature/message/sql/requete
    print("SQL_REQ: " + msg.topic + " " + str(msg.qos) + " " + str(msg.payload))
    
def on_message_texte(self, obj, msg):
    # This callback will only be called for messages with topics that match
    # ossature/message/sql/requete
    print("TEXTE: " + msg.topic + " " + str(msg.qos) + " " + str(msg.payload))

def on_message(self, obj, msg):
    # This callback will be called for messages that we receive that do not
    # match any patterns defined in topic specific callbacks
    print("MESSAGE_NON_CONFORME: " + msg.topic + " " + str(msg.qos) + " " + str(msg.payload))

def payload_byteToList(payload_byte):
    '''permet de parser dans une liste les différentes parties de la charge utile'''
    #convertir un tableau de byte en str
    payload_str = payload_byte.decode('UTF-8')
    liste = payload_str.split(';')
    return liste

   
if __name__ == "__main__":   
    try:
        mqttc = mqtt.Client()
        # Add message callbacks that will only trigger on a specific subscription match.
        mqttc.message_callback_add("ossature/capteur/temperature/#", on_message_capteur_temperature)
        mqttc.message_callback_add("ossature/capteur/hr/#", on_message_capteur_hr)
        mqttc.message_callback_add("ossature/capteur/pression/#", on_message_capteur_pression)
        mqttc.message_callback_add("ossature/actionneur/voyant/#", on_message_actionneur)
        mqttc.message_callback_add("ossature/actionneur/moteur/#", on_message_actionneur)
        mqttc.message_callback_add("ossature/message/sql/requete/#", on_message_sql_req)
        mqttc.message_callback_add("ossature/message/texte/#", on_message_texte)
        mqttc.on_message = on_message
        mqttc.connect(broker, 1883, 60)
        mqttc.subscribe("ossature/#", 0)
        while True:
            mqttc.loop(0.1)# gestion des buffers
            
            
    except KeyboardInterrupt:# CTRL+C sous linux
        time.sleep(0.1)
        print("_____FIN______")
