#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Publier et répéter des topics avec horodatage
#        projet ossature


import paho.mqtt.publish as publish
import time
import datetime

############choisir le broker######
broker= "mqtt.eclipse.org"
#broker = "localhost"
###################################

if __name__ == "__main__":   
    try:
        #ici code exécuté une fois
        while True:# thread principal pour répéter des envois de sujet
            time.sleep(2)
            date = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S.%f')
            publish.single("ossature/message/texte/requete", "select * from table" + ';' + date, hostname=broker)
            publish.single("ossature/capteur/temperature/t1", "12.5" + ';' + date, hostname=broker)
        
            
    except KeyboardInterrupt:# CTRL+C sous linux
        time.sleep(0.1)
        print("_____FIN______")