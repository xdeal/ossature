#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# un exemple pour souscrire et publier tous les topics du projet ossature
# qui permet de choisir le nombre de clients de chaque groupe
# ainsi que les valeurs du payload:
#    soit les valeurs sont prises au hasard entre valeur min et max
#    soit c'est une chaine de caractère
# multi-threading des groupes
# horodatage des publications
#
#           projet ossature


import paho.mqtt.client as mqtt
import threading
import time
import random
import datetime

############ choisir le broker ######
#broker= "mqtt.eclipse.org"
broker = "localhost"
#####################################

class MyMQTTClass(mqtt.Client):
    def on_connect(self, mqttc, obj, flags, rc):
        #print("____on_connect rc: "+str(rc)+" self:   "+self)
        pass     
    def on_disconnect(self, mqttc, obj, flags):
        #print("____________deconnection: "+ str(mqttc))
        pass
    def on_message(self, mqttc, obj, msg):
        print("========on_message: "+msg.topic+" "+str(msg.qos)+" "+str(msg.payload))
    def on_publish(self, mqttc, obj, mid):
        #print("__on_publish mid: "+str(mid))
        pass
    def on_subscribe(self, mqttc, obj, mid, granted_qos):
        #print("____on_subscribe Subscribed: "+str(mid)+" "+str(granted_qos))
        pass
    # def on_log(self, mqttc, obj, level, string):
    #     print("log parle: ",string)
    def run(self,broker,port,sub_topic,pub_topic,pub_payload):
        self.connect(broker, port, 60)
        self.subscribe(sub_topic, 0)
        self.loop_start()#non bloquant gere les queues et lance un thread
        self.publish(pub_topic,pub_payload)
        
class TopicGroup(threading.Thread):
    def __init__(self,topic="",nbClients=1,payload_min=0,payload_max=0,payload_txt="parDefaut",port=1883,broker="localhost",name="blank"):
        '''
        nbClients: echelle du topic 
        payload: pour transmettre une valeur entre min et max prise au hasard
                 si min=0 et max=0 alors le texte sera emit (config par défaut)
        port et broker: choix du broker mqtt
        '''
        threading.Thread.__init__(self)
        self.topic = topic
        self.nbClients = nbClients
        self.range = range
        self.port = port
        self.broker = broker
        self.name = name
        self.payload_min = payload_min
        self.payload_max = payload_max
        self.payload_txt = payload_txt
        self.clients = []
        self.clientsMqtt=[]#liste des clients  
    def run(self):
        '''méthode threader de création d'un groupe de clients puis souscription et  publication'''
        for i in range(1,self.nbClients+1):
            if self.payload_min ==0 and self.payload_max == 0:
                payload = self.payload_txt
            else:
                payload=random.randint(self.payload_min,self.payload_max)
            date = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S.%f')
            payload_str = str(payload) +";" +date
            self.clients.append({"broker":self.broker,"port":self.port,"name":self.name,"topic":self.topic+str(i),"payload":payload_str})       
        #creer objets clients
        for c in self.clients:
            print("********création du client",c['topic'])
            mqttc = MyMQTTClass()
            mqttc.run(c['broker'],c['port'],c["topic"],c["topic"],c["payload"])
            self.clientsMqtt.append(mqttc) 
    def off(self):
        '''méthode permettant la déconnection du groupe'''
        for c in self.clientsMqtt:
            c.disconnect()


if __name__ == "__main__":   
    try:#pour des envois uniques
        #creer le groupe
        groupeTemperaturet=TopicGroup(topic="ossature/capteur/temperature/t",nbClients=1,payload_min=4,payload_max=45,broker=broker) 
        #demarre le thread groupe
        groupeTemperaturet.start()
        groupePressionp = TopicGroup("ossature/capteur/pression/p",1,2000,4000,broker=broker)
        groupePressionp.start()
        groupeHr = TopicGroup("ossature/capteur/hr/hr",1,20,40,broker=broker)
        groupeHr.start()
        groupeActionneurV = TopicGroup("ossature/actionneur/voyant/v",1,1,1,broker=broker)
        groupeActionneurV.start()
        groupeActionneurM = TopicGroup("ossature/actionneur/moteur/m",1,1,1,broker=broker)
        groupeActionneurM.start()
        groupeSQL = TopicGroup("ossature/message/sql/requete/r",1,payload_txt="select * from table",broker=broker)
        groupeSQL.start()
        groupeMessage = TopicGroup("ossature/message/texte/t",1,payload_txt="Bonjour",broker=broker)
        groupeMessage.start()
        while True:# thread principal pour répéter des envois de sujet
            time.sleep(2)
            no_threads=threading.active_count()
            print("current threads =",no_threads)
            
    except KeyboardInterrupt:# CTRL+C sous linux
        #deconnecter les clients du groupe
        groupeTemperaturet.off()
        groupePressionp.off()  
        groupeHr.off()
        groupeActionneurV.off()
        groupeActionneurM.off()
        groupeSQL.off()
        groupeMessage.off()
    time.sleep(1)
    print("_____FIN______")
        
    
