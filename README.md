Benmammar Nabil 

Berchoux Michael 

Deal Xavier

Fousse Frederic

_____________________________________________________
**Projet ossature IoT**
____________________________________________________


***Introduction:***

Le sujet concerne plusieurs parties du programme de terminale et permettra de fournir l’ossature fonctionnelle d’un projet de NSI.  Il prend comme support les IoT et autres objets connectés.
Il est réutilisable en NSI, STI2D-SIN , STI2D-EE et SSI en tant que base de projet ou support de TP/TD/Cours.

* type de support: base pour des **projets** mettant en oeuvre des IoT.

* durée pour les élèves: environ 8 semaines en fin de terminale.

* pré-requis des élèves: définis dans le document *Cadrage/presentationProjet/presentationOssature.pdf*.



***Présentation du projet:***

Pour une première approche, vous trouverez dans le dossier *Cadrage/presentationProjet* une vidéo de présentation du projet ainsi que les documents utilisés. Les autres dossiers permettent de mettre en oeuvre les différentes parties du projet.

***Cas d’utilisation:***

Une base de données Sqlite enregistre les mesures (T°, Humidity Rate, Pression) issues de différents capteurs réels (ESP8266 ou ESP32 fonctionnant sous micropython) et/ou de capteurs virtuels (client sous python publiant des valeurs). Un serveur (Flask) permet d’afficher les résultats de cette base à travers une simple interface utilisateur écrit en HTML-CSS-Javascript. L’ensemble communique par le protocole MQTT.
Le tout est presque entièrement écrit en python et met en œuvre de nombreux points du programme de NSI. 
Une ossature fonctionnelle sans raffinement permettra de servir de base de travail pour de futurs projets.
