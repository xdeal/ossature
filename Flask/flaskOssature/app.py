#!/usr/bin/env python
# -*- coding: utf-8 -*-

# projet ossature
# serveur Flask
# donne accès à une base de données déportées
# à l'aide du protocole MQTT
# permet de commander un voyant sur un client IoT MQTT
# et de recevoir des mesures.

import eventlet
import json
import logging
import time
from flask import Flask, render_template, request, abort, redirect, url_for
from flask_socketio import SocketIO 
from flask_mqtt import Mqtt
from tabulate import tabulate

table=["Ne pas oublier de lancer clientPubSub_SQL.py!"]
eventlet.monkey_patch()

# mettre level=logging.DEBUG pour avoir plus d'informations
#logging.basicConfig(level=logging.INFO)
logging.basicConfig(level=logging.INFO)

app = Flask(__name__)
app.config['SECRET_KEY'] = 'secretsocket'
###### choisir le broker ##########################
app.config['MQTT_BROKER_URL'] = "mqtt.eclipse.org"
#app.config['MQTT_BROKER_URL'] = 'localhost'
###################################################
app.config['MQTT_BROKER_PORT'] = 1883
app.config['MQTT_USERNAME'] = ''
app.config['MQTT_PASSWORD'] = ''
app.config['MQTT_REFRESH_TIME'] = 1.0  # refresh time in seconds
app.config['TEMPLATES_AUTO_RELOAD'] = True
app.config['MQTT_KEEPALIVE'] = 5
app.config['MQTT_TLS_ENABLED'] = False
app.config['MQTT_CLEAN_SESSION'] = True

mqtt = Mqtt(app)
socketio = SocketIO(app)

@app.route("/")
def index():
    return render_template('index.html')

def classw3(table): # permet d'utiliser la table w3.css
    t1=table.replace("<table>",'<table class="w3-table-all">')
    t2=t1.replace("<thead><tr>",'<thead><tr class="w3-red">')   
    return t2


@app.route('/requete/', methods=('GET', 'POST'))
def requete():
    if request.method == 'POST':
        content = request.form['content']
        logging.debug("content app.py: %s ", content)
        mqtt.publish('ossature/message/sql/requete/req1', content)
        time.sleep(2)# on attend la réponse
        table1 = json.loads(table) # json.loads( ) pour deserialiser
        table2 = tabulate(table1, tablefmt='html')
        logging.debug("table2: %s ", table2)
        table3 = classw3(table2)
        logging.debug("table3: %s ", table3)
        return render_template('requete.html',table=table3)
    else:
        return render_template('requete.html',table=table)
        
@app.route("/tableau/")
def tableau():
    return render_template('tableau.html')

#### serveur-client navigateur websocket debut#######
@socketio.on('messageLed')
def test_message(json_str):
    data = json.loads(json_str) #JSON to Python (Decoding) with loads
    mqtt.publish(data['topic'], data['payload'])# vers broker
    socketio.emit('reponseLed',data=data )      # vers navigateur
    logging.debug("test_message-messageLed: %s ", data)
    
@socketio.on('connect')
def test_connect():
    print("test_connect")
    socketio.emit('reponse', {'data': 'serveur:Connected'})

@socketio.on('disconnect')
def test_disconnect():
    print('disconnect')
#### serveur-client navigateur websocket fin #######


#### MQTT-serveur websocket début #######
#https://github.com/stlehmann/Flask-MQTT
mqtt.subscribe('ossature/message/sql/reponse/#')
mqtt.subscribe('ossature/capteur/temperature/t1')
@mqtt.on_message()
def handle_mqtt_message(client, userdata, message): 
    global table
    data = dict(
        topic=message.topic,
        payload=message.payload.decode(), #decode(o) – Same as json.loads() method return Python data structure of JSON string or data.
        qos=message.qos)
    logging.debug("data de MQTT: %s ",  data)    
    if data["topic"]=='ossature/capteur/temperature/t1':
        temperature = data["payload"].split(';')[0]#prend le premier terme de la chaine
        socketio.emit('temperature_t1',data= temperature)
        logging.debug("MQTT data reçu du topic: %s  contenu: %s ",  data["topic"], data["payload"].decode())    
    if data["topic"]=='ossature/message/sql/reponse/r1':   
        table = data["payload"]
        print("table_reponse_sql: ",table)
        #table = message.payload.decode()
        logging.debug("contenu de table: %s  ",  table)
#### MQTT-serveur websocket fin #######


if __name__ == "__main__":
    # important: Do not use reloader because this will create two Flask instances.
    # Flask-MQTT only supports running with one instance
    #socketio.run(app, host='127.0.0.1', port=5000, use_reloader=False, debug=True)
    socketio.run(app, host='0.0.0.0', port=5000, use_reloader=False, debug=True)
