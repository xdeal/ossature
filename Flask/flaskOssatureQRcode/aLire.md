## Ossature serveur Flask QRcode
Le serveur Flask est le pont entre deux types de clients différents:
1. un coté client MQTT constitué d'une base de donnée sqlite3, de clients IoT à base d'ESP32.
2. un coté client de type navigateur web permettant d'effectuer des requêtes SQL (méthode POST/GET) avec le serveur ou de commander un voyant d'un capteur IoT  et de recevoir les mesures du capteur (méthode websocket avec le serveur Flask).
3. Les QRcodes permettent au smartphone de se connecter plus facilement au point d'accès wifi ainsi qu'au serveur Flask.


Pour les essais du serveur Flask avec la base de donnée ossature.db, il faut que le client clientPubSub_SQL.py soit lancé préalablement.
*Vérifier que le broker MQTT soit identique pour tous les clients.*

* pour le coté cosmétique des pages web la bibliothèque [w3.css](https://www.w3schools.com/w3css/) sera utilisée.
______
### pense-bête et références:
[flask-socketio](https://flask-socketio.readthedocs.io/en/latest/)
[flask-mqtt](https://github.com/stlehmann/Flask-MQTT)

installer websocket:
pip install eventlet
pip install Flask-socketIO
pip install flask_login
pip install flask-mqtt

pip install flask-qrcode




